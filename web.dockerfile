FROM nginx:latest

COPY ./config/nginx/conf.d/app.conf /etc/nginx/conf.d/app.conf

RUN apt update && apt -y upgrade && apt install -y curl mime-support nodejs npm

RUN npm install -g @quasar/cli